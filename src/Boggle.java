import java.util.*;
import java.io.*;

public class Boggle {	
	public static void main(String[] args) throws Exception {
		
		BufferedReader infile = new BufferedReader(new FileReader("dictionary.txt"));
		
		String[][] board = createBoard(args[0]);
		HashSet<String> dict = new HashSet<String>();
		HashSet<String> subStrs = new HashSet<String>();
		TreeSet<String> found = new TreeSet<String>();
		
		while(infile.ready())
			dict.add(infile.readLine());
		infile.close();

		createSubs(dict, subStrs);
		
		for(int r = 0; r < board.length; r++)
			for(int c = 0; c < board.length; c++)
				dfs(r, c, board, "", dict, subStrs, found);
		
		for(String word: found)
			System.out.println(word);
	}
	
	private static void dfs(int r, int c, String[][] board, String word, HashSet<String> dict, HashSet<String> subStrs, TreeSet<String> found) {		
		
		word += board[r][c];
		
		if(word.length() >= 3 && dict.contains(word))
			found.add(word);
		
		if(!subStrs.contains(word))
			return;
		
		if (r != 0 && Character.isLowerCase(board[r - 1][c].charAt(0))) {
 			board[r][c] = board[r][c].toUpperCase();
 			dfs(r - 1, c, board, word, dict, subStrs, found);
 			board[r][c] = board[r][c].toLowerCase();
 		}

		if (r != 0 && c != board.length - 1 && Character.isLowerCase(board[r - 1][c + 1].charAt(0))) {
			board[r][c] = board[r][c].toUpperCase();
			dfs(r - 1,c + 1, board, word, dict, subStrs, found);
			board[r][c] = board[r][c].toLowerCase();
		}

		if (c != board.length - 1 && Character.isLowerCase(board[r][c + 1].charAt(0))) {
			board[r][c] = board[r][c].toUpperCase();
			dfs(r,c + 1, board, word, dict, subStrs, found);
			board[r][c] = board[r][c].toLowerCase();
		}

		if (r != board.length - 1 && c != board.length - 1 && Character.isLowerCase(board[r + 1][c + 1].charAt(0))) {
			board[r][c] = board[r][c].toUpperCase();
			dfs(r + 1,c + 1, board, word, dict, subStrs, found);
			board[r][c] = board[r][c].toLowerCase();
        }

		if (r != board.length - 1 && Character.isLowerCase(board[r + 1][c].charAt(0))) {
			board[r][c] = board[r][c].toUpperCase();
			dfs(r + 1, c, board, word, dict, subStrs, found);
			board[r][c] = board[r][c].toLowerCase();
        }

		if (r != board.length - 1 && c != 0 && Character.isLowerCase(board[r + 1][c - 1].charAt(0))) {
			board[r][c] = board[r][c].toUpperCase();
			dfs(r+1,c - 1, board, word, dict, subStrs, found);
			board[r][c] = board[r][c].toLowerCase();
        }

        if (c != 0 && Character.isLowerCase(board[r][c - 1].charAt(0))) {
			board[r][c] = board[r][c].toUpperCase();
			dfs(r,c - 1, board, word, dict, subStrs, found);
			board[r][c] = board[r][c].toLowerCase();
        }

        if (r != 0 && c != 0 && Character.isLowerCase(board[r - 1][c - 1].charAt(0))) {
			board[r][c] = board[r][c].toUpperCase();
			dfs(r - 1,c - 1, board, word, dict, subStrs, found);
			board[r][c] = board[r][c].toLowerCase();
        }
	}
	
	
	private static String[][] createBoard (String size) throws Exception {
		
		Scanner infile = new Scanner(new FileReader(size));
		int rows = infile.nextInt(), cols = rows;
		String[][] board = new String[rows][cols];
		
		for(int r = 0; r < rows; r++)
			for(int c = 0; c < cols; c++)
				board[r][c] = infile.next();
		infile.close();
		
		return board;
	}
		
	private static void createSubs(HashSet<String> dict, HashSet<String> subStrs) {
		
		String sub = new String();
		for(String s: dict) {
			
			sub = "";
			for(int i = 0; i < s.length(); i++) {
				
				sub += s.charAt(i);
				if(!subStrs.contains(sub))
					subStrs.add(sub);
			}
		}
	}
}